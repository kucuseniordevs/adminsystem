alembic==1.0.2
Click==7.0
decorator==4.3.0
dominate==2.3.4
Flask==1.0.2
Flask-Bootstrap==3.3.7.1
Flask-Login==0.4.1
Flask-Migrate==2.3.0
Flask-SQLAlchemy==2.3.2
Flask-Testing==0.7.1
Flask-WTF==0.14.2
infinity==1.4
intervals==0.8.1
itsdangerous==1.1.0
Jinja2==2.10
Mako==1.0.7
MarkupSafe==1.1.0
mysqlclient==1.3.13
pkg-resources==0.0.0
python-dateutil==2.7.5
python-editor==1.0.3
six==1.11.0
SQLAlchemy==1.2.14
SQLAlchemy-Utils==0.33.6
validators==0.12.3
visitor==0.1.3
Werkzeug==0.14.1
WTForms==2.2.1
WTForms-Alchemy==0.16.7
WTForms-Components==0.10.3
