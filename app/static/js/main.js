// This function autofills the amount field for the budget form for client view
var amountInput = document.getElementById('amount')
var displayItemAmount = (
    amountInput.addEventListener('focus', function (event) {
        var budgetForm = document.getElementById('budget')
        var itemPrice = document.getElementById('price').value
        var itemQuantity = document.getElementById('quantity').value
        if (budgetForm && itemPrice && itemQuantity) {
            amountInput.value = itemPrice * itemQuantity
            amountInput.setAttribute('readonly', '')
        }
    })
)()
document.addEventListener("DOMContentLoaded", function(event) {
if (amountInput) {
    displayItemAmount();
}
});

// Change input type text to number for price, quantity and amount fields
document.addEventListener("DOMContentLoaded", function(event) {
var budgetForm = document.getElementById('budget')
var priceInput = document.getElementById('price')
var quantityInput = document.getElementById('quantity')
var amount = document.getElementById('amount')
if(budgetForm) {
    priceInput.setAttribute('type', 'number')
    quantityInput.setAttribute('type', 'number')
    amountInput.setAttribute('type', 'number')
}
});


(function ($) {
    "use strict";


     /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);

// End of function

// This function adds a close button on flashed messeges
// var closeFlashMessage = (function(){
//     alertElements = document.getElementsByClassName("alert")
//     if(alertElements){
//         for (let i = 0; i < alertElements.length; i++) {
//             alertElements[i].classList.add("close")
//         }
//     }
// }
// )()
// closeFlashMessage()
// End of function