from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, TextAreaField, SubmitField, ValidationError, SelectField, HiddenField
from wtforms.validators import DataRequired

from ..models import BudgetItem, ItemCategory


class BudgetItemForm(FlaskForm):
    """Form for treasures adding items to a budget"""

    budget = HiddenField(validators=[DataRequired()])
    category = SelectField('Category', choices=[('', 'Select')]+[(name, member.value) for name, member in ItemCategory.__members__.items()], validators=[DataRequired()])
    item_name = StringField('Name of Item/Service', validators=[DataRequired()])
    description = TextAreaField('Use/Need for Item/Service Description', validators=[DataRequired()])
    price = IntegerField('Cost per Item/Service', validators=[DataRequired()])
    quantity = IntegerField('Number of Items/Services')
    amount = IntegerField('Amount')
    submit = SubmitField('Add')