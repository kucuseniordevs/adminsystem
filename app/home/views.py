from flask import abort, flash, render_template, redirect, url_for
from flask_login import current_user, login_required
from sqlalchemy import func

from .. import db
from ..models import Member, BudgetItem, Budget, Department, Role
from ..auth.forms import MemberRegistrationForm
from .forms import BudgetItemForm
from . import home


def check_role():
    """Find out the role of the currently logged in member"""
    # should be in all route methods (apart from those for admin only) because it is used in the base.html
    if current_user.is_authenticated and not current_user.is_admin:
        member = Member.query.filter_by(email=current_user.email).first()
        check_role.role_name = member.role.name


def check_treasurer():
    """To make sure only a treasurer can use this route"""
    if current_user.role.name != 'Treasurer':
        abort(403)


def check_coordinator():
    """To make sure only a coordinator can use this route"""
    if current_user.role.name != 'Coordinator':
        abort(403)


def check_secretary():
    """To make sure only a secretary can use this route"""
    if current_user.role.name != 'Secretary':
        abort(403)


def check_prayer_secretary():
    """To make sure only a prayer secretary can use this route"""
    if current_user.role.name != 'Prayer Secretary':
        abort(403)


def check_dept_category():
    """Check the department category of the current user to filter page viewing"""
    category = current_user.department.category.name
    if category == 'committee':
        return False
    # elif current_user.department.category == 'subcommittee':
    #     return True
    else:
        return True


@home.route('/member/profile/<int:id>')
@login_required
def profile(id):
    """Route for viewing profile"""
    member = Member.query.get_or_404(id)
    if current_user.id != member.id:
        abort(403)
    check_role()
    role_name = check_role.role_name
    return render_template('home/profile.html', id=member.id, member=member, role_name=role_name, active="profile")


@home.route('/member/profile/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def update_profile(id):
    """Route for updating profile"""
    check_role()
    role_name = check_role.role_name
    member = Member.query.get_or_404(id)
    if (current_user.id != member.id) or not member.is_leader:
        abort(403)
    form = MemberRegistrationForm(obj=member)
    if form.validate_on_submit():
        member.firstname = form.firstname.data
        member.lastname = form.lastname.data
        member.registration_no = form.registration_no.data
        member.phone = form.phone.data
        member.email = form.email.data
        # member.password = form.password.data
        db.session.commit()
        return redirect(url_for('home.profile', id=current_user.id))
    form.firstname.data = member.firstname
    form.lastname.data = member.lastname
    form.registration_no.data = member.registration_no
    form.phone.data = member.phone
    form.email.data = member.email
    # form.password.label = "Current Password"
    # form.password.data = member.password
    return render_template('auth/member_register.html', role_name=role_name, form=form, title='Profile update', active="edit_profile")


@home.route('/treasurer/dashboard')
@login_required
def treasurer_dashboard():
    """Render the treasurers' dashboard"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    # if the department is not KUCU
    if check_dept_category()==False:
        return render_template('home/dashboards/treasurers/treasurer.html', role_name=role_name, title="Treasurer", active="dashboard")
    departments = Department.query.all()
    budgets = Budget.query.all()
    number = Budget.query.count()
    treasurers = Member.query.filter_by(role_id=current_user.role.id).all()
    treasurers_count = Member.query.filter_by(role_id=current_user.role.id).count()
    budgets_amount = db.session.query(func.sum(Budget.amount)).scalar()

    return render_template('home/dashboards/exec_treasurer.html', departments=departments, role_name=role_name, budgets=budgets, budgets_amount=budgets_amount, treasurers = treasurers, treasurers_count=treasurers_count,  budgets_number = number,  title="Executive Treasurer", active="dashboard")


@home.route('/treasurer/dashboard/budgets/<int:id>')
@login_required
def list_budgets(id):
    """List all budgets of a department or the whole union"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    department = Department.query.get_or_404(id)
    if department.name == 'KUCU':
        budgets = Budget.query.all()
    else:
        budgets = Budget.query.filter_by(department_id=department.id).all()
    return render_template('home/dashboards/treasurers/budgets.html', role_name=role_name, budgets=budgets, department=department, id=department.id, title="Budgets", active="budgets")


@home.route('/treasurer/dashboard/budgets/add/<int:id>', methods=['GET', 'POST'])
@login_required
def add_budget(id):
    """Route that adds a new budget's details of a department to table"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    if check_dept_category():
        abort(403)
    department = Department.query.get_or_404(id)
    budgets = Budget.query.filter_by(department_id=department.id).all()
    if budgets:
        for budget in budgets:
            if not budget.is_submitted:
                flash("{} has not been submitted. Kindly add new items to it.".format(budget.name))
                return redirect(url_for('home.view_budget', id=budget.id))
    
    number = Budget.query.filter_by(department_id=department.id).count()
    budget = Budget(name='{}Budget{}'.format(current_user.department.name, number+1), member_id = current_user.id, department_id = current_user.department.id, amount=0)
    # This is because budget wont have an id until it is committed
    db.session.add(budget)
    db.session.commit()
    return redirect(url_for('home.view_budget', id=budget.id))


@home.route('/treasurer/dashboard/budget/submit/<int:id>', methods=['GET', 'POST'])
@login_required
def submit_budget(id):
    """Route that submits a new budget's details to table"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    if check_dept_category():
        abort(403)

    budget = Budget.query.get_or_404(id)
    budget.is_submitted = True
    db.session.commit()
    return redirect(url_for('home.list_budgets', id=budget.department.id))


@home.route('/treasurer/dashboard/budget/<int:id>', methods=['GET', 'POST'])
@login_required
def view_budget(id):
    """View a budget on adding/submitted status"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    budget = Budget.query.get_or_404(id)
    budget_items = BudgetItem.query.filter_by(budget_id=budget.id).all()
    return render_template('home/dashboards/treasurers/budget.html', role_name=role_name, id=budget.id, budget=budget, budget_items=budget_items, title="BudgetItems", active="budgets")


@home.route('/treasurer/dashboard/budget/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_budget(id):
    """Route that deletes the budget being added currently(not yet submitted)"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    if check_dept_category():
        abort(403)

    budget = Budget.query.get_or_404(id)
    department_id = budget.department.id
    budget_items = BudgetItem.query.filter_by(budget_id=budget.id).all()
    # budget_items = Budget.query.join(budget_items)
    for budget_item in budget_items:
        db.session.delete(budget_item)
    db.session.delete(budget)
    db.session.commit()
    flash('You have successfully deleted the budget')

    # redirect to budgets page
    return redirect(url_for('home.list_budgets', id=department_id))


@home.route('/treasurer/dashboard/budget/add_items/<int:id>', methods=['GET', 'POST'])
@login_required
def add_budget_items(id):
    """Render the treasurer's new budget items entry page"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    if check_dept_category():
        abort(403)

    budget = Budget.query.get_or_404(id)

    budget_items = BudgetItem.query.filter_by(budget_id=budget.id).all()
    form = BudgetItemForm()
    form.budget.data = budget
    if form.validate_on_submit():
        item_amount = form.price.data*form.quantity.data
        form.amount.data = item_amount
        budget_item = BudgetItem(budget=form.budget.data, category=form.category.data, name=form.item_name.data, description=form.description.data, price=form.price.data, quantity=form.quantity.data, amount=item_amount)
        db.session.add(budget_item)
        db.session.commit()
        budget.amount = db.session.query(func.sum(BudgetItem.amount)).filter(BudgetItem.budget_id==budget.id)
        db.session.commit()
        flash('Successfully Added Item to Budget')
        return redirect(url_for('home.view_budget', id=budget.id))
    return render_template('home/dashboards/treasurers/budgetform.html', form=form, role_name=role_name, budget=budget, budget_items=budget_items, title="New Budget", active="budgets")


@home.route('/treasurer/dashboard/budget/edit_items/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_budget_items(id):
    """Render the treasurer's budget items editing page"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    if check_dept_category():
        abort(403)
    budget_item = BudgetItem.query.get_or_404(id)

    form = BudgetItemForm(obj=budget_item)
    if form.validate_on_submit():
        budget_item.category = form.category.data
        budget_item.name = form.item_name.data
        budget_item.description = form.description.data
        budget_item.price = form.price.data
        budget_item.quantity = form.quantity.data
        budget_item.amount = form.amount.data
        db.session.commit()
        budget_id = db.session.query(BudgetItem.budget_id).filter(BudgetItem.id==budget_item.id).first()
        budget = Budget.query.get_or_404(budget_id)
        budget.amount = db.session.query(func.sum(BudgetItem.amount)).filter(BudgetItem.budget_id==budget.id)
        db.session.commit()
        flash('You have successfully edited the budget\'s items')
        # redirect to budget page
        return redirect(url_for('home.view_budget', id=budget.id))
    form.category.data = budget_item.category
    form.item_name.data = budget_item.name
    form.description.data = budget_item.description
    form.price.data = budget_item.price
    form.quantity.data = budget_item.quantity
    form.amount.data = budget_item.amount
    return render_template('home/dashboards/treasurers/budgetform.html', form=form, role_name=role_name, budget_item=budget_item, title="Edit Budget Item", active="budgets")


@home.route('/treasurer/dashboard/budget/delete_items/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_budget_items(id):
    """Delete budget items"""
    check_treasurer()
    check_role()
    role_name = check_role.role_name
    if check_dept_category():
        abort(403)

    budget_item = BudgetItem.query.get_or_404(id)
    budget_id = db.session.query(BudgetItem.budget_id).filter(BudgetItem.id==budget_item.id).first()
    budget = Budget.query.get_or_404(budget_id)
    db.session.delete(budget_item)
    db.session.commit()
    budget.amount = db.session.query(func.sum(BudgetItem.amount)).filter(BudgetItem.budget_id==budget.id)
    db.session.commit()
    flash('You have successfully deleted the item')
    # redirect to budget page
    return redirect(url_for('home.view_budget', id=budget.id))


@home.route('/exectreasurer/dashboard/budget/approve/<int:id>')
@login_required
def approve_budget(id):
    """Route for executive treasurer to approve a budget"""
    check_treasurer()
    if check_dept_category()==False:
        abort(403)
    # make sure only executive treasurer access this page
    check_role()
    role_name = check_role.role_name
    budget = Budget.query.get_or_404(id)
    budget.is_approved = True
    db.session.commit()
    return redirect(url_for('home.list_budgets', id=budget.department.id))


@home.route('/exectreasurer/dashboard/budget/review/<int:id>')
@login_required
def review_budget(id):
    """Route for executive treasurer to review a budget by enabling it to be edited"""
    check_treasurer()
    if check_dept_category()==False:
        abort(403)
    # make sure only executive treasurer access this page
    check_role()
    role_name = check_role.role_name
    budget = Budget.query.get_or_404(id)
    budget.is_submitted = False
    db.session.commit()
    return redirect(url_for('home.list_budgets', id=budget.department.id))


@home.route('/coordinator/dashboard')
@login_required
def coordinator_dashboard():
    """Render the coordinator's dashboard"""
    check_coordinator()
    check_role()
    role_name = check_role.role_name
    return render_template('home/dashboards/coordinator.html', role_name=role_name, title="Coordinator")


@home.route('/secretary/dashboard')
@login_required
def secretary_dashboard():
    """Render the secretary's dashboard"""
    check_secretary()
    check_role()
    role_name = check_role.role_name
    return render_template('home/dashboards/secretary.html', role_name=role_name, title="Secretary")


@home.route('/prayersecretary/dashboard')
@login_required
def prayer_secretary_dashboard():
    """Render the secretary's dashboard"""
    check_prayer_secretary()
    check_role()
    role_name = check_role.role_name
    return render_template('home/dashboards/prayer_secretary.html', role_name=role_name, title="Prayer Secretary")


@home.route('/admin/dashboard')
@login_required
def admin_dashboard():
    # prevent non-admins from accessing the page
    if not current_user.is_admin:
        abort(403)

    total_roles = Role.query.count()
    members = Member.query.all()
    total_members = Member.query.count()
    total_departments = Department.query.count()
    for member in members:
        total_leaders = Member.query.filter_by(is_leader=True).count()
    return render_template('home/admin_dashboard.html', title="Super Admin", total_departments=total_departments, total_members=total_members, total_roles=total_roles, total_leaders=total_leaders, active="dashboard")
