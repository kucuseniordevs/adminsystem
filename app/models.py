from enum import Enum, unique
from datetime import datetime
from sqlalchemy.sql import func
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login_manager


@unique
class DepartmentCategory(Enum):
    """The various categories of departments"""
    general = 'General'
    committee = 'Committee'
    subcommittee = 'Subcommittee'


@unique
class ItemCategory(Enum):
    product = 'Product'
    service = 'Service'


class Member(UserMixin, db.Model):
    """KUCU members table"""
    __tablename__ = 'members'

    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(60), index=True)
    lastname = db.Column(db.String(60), index=True)
    registration_no = db.Column(db.String(60), index=True, unique=True)
    phone = db.Column(db.String(10), index=True)
    email = db.Column(db.String(60), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    is_leader = db.Column(db.Boolean, default=False)
    is_admin = db.Column(db.Boolean, default=False)
    is_superadmin = db.Column(db.Boolean, default=False)
    budgets = db.relationship('Budget', backref='member', lazy='dynamic')
    # other details such as residence will be included

    @property
    def password(self):
        """Prevent password from being accessed"""
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        """Set password to a hashed one"""
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """Check if hashed password matches actual password"""
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<member: {}>'.format(self.firstname)


# set up user_loader
@login_manager.user_loader
def load_user(user_id):
    return Member.query.get(int(user_id))


class Department(db.Model):
    """Create a table for departments in KUCU"""

    __tablename__ = 'departments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    category = db.Column(db.Enum(DepartmentCategory, validate_strings=True))
    description = db.Column(db.String(200))
    members = db.relationship('Member', backref='department', lazy='dynamic')
    budgets = db.relationship('Budget', backref='department', lazy='dynamic')

    def __repr__(self):
        return '<Department: {}>'.format(self.name)


class Role(db.Model):
    """Create a role table"""

    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    members = db.relationship('Member', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role: {}>'.format(self.name)


class Budget(db.Model):
    """Create a table for budgets"""

    __tablename__ = 'budgets'

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, server_default=func.now())
    name = db.Column(db.String(60), unique=True)
    member_id = db.Column(db.Integer, db.ForeignKey('members.id'))
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
    amount = db.Column(db.Integer)
    budget_items = db.relationship('BudgetItem', backref='budget', lazy='dynamic')
    is_submitted = db.Column(db.Boolean, default=False)
    is_approved = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<Budget: {}>'.format(self.name)


class BudgetItem(db.Model):
    """Create table for the items in budgets"""

    __tablename__ = 'budgets_items'

    id = db.Column(db.Integer, primary_key=True)
    budget_id = db.Column(db.Integer, db.ForeignKey('budgets.id'))
    category = db.Column(db.Enum(ItemCategory))
    name = db.Column(db.String(60))
    price = db.Column(db.Integer)
    description = db.Column(db.String(200))
    quantity = db.Column(db.Integer)
    amount = db. Column(db.Integer)

    def __repr__(self):
        return '<BudgetItem: {}>'.format(self.name)
