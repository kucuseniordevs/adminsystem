from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, ValidationError, SelectField, IntegerField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms_alchemy.fields import QuerySelectField

from ..models import Member, Department, Role, DepartmentCategory


class MemberRegistrationForm(FlaskForm):
    """Form for anyone to register as a member"""

    firstname = StringField('Firstname', validators=[DataRequired()])
    lastname = StringField('Lastname', validators=[DataRequired()])
    registration_no = StringField('Registration Number', validators=[DataRequired()])
    phone = IntegerField('Phone Number', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Register')


class LeaderRegistrationForm(FlaskForm):
    """Form for leaders to be registered"""

    # choices = [('', 'Select')]
    email = StringField('Email', validators=[DataRequired(), Email()])
    department = QuerySelectField(query_factory=lambda: Department.query.all(), get_label="name", allow_blank=True, blank_text="Select", validators=[DataRequired()])
    role = QuerySelectField(query_factory=lambda: Role.query.all(), get_label="name", allow_blank=True, blank_text="Select", validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('confirm_password')])
    confirm_password = PasswordField('Confirm Password')
    submit = SubmitField('Register as Leader')

    # def validate_email(self, field):
    #     if Member.query.filter_by(email=field.data).first():
    #         raise ValidationError('Email is already in use')


class LoginForm(FlaskForm):
    """Form for leaders to login"""
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')
