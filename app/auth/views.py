from flask import flash, redirect, render_template, url_for, abort, request
from flask_login import login_required, login_user, current_user, logout_user

# Beware of circular imports
from . import auth
from .. import db
from ..models import Member
from ..home.views import check_role
from ..auth.forms import LoginForm, MemberRegistrationForm, LeaderRegistrationForm


@auth.route('/member/register', methods=['GET', 'POST'])
def member_register():
    form = MemberRegistrationForm()
    if form.validate_on_submit():
        member = Member(firstname=form.firstname.data, lastname=form.lastname.data, registration_no=form.registration_no.data, phone=form.phone.data, email=form.email.data, is_leader=False, is_admin=False)

        db.session.add(member)
        db.session.commit()
        return redirect(url_for('auth.login'))
        
    # load the member registration template
    return render_template('auth/member_register.html', form=form, title='Member Register', active="register")


@auth.route('/leader/register', methods=['GET', 'POST'])
def leader_register():
    form = LeaderRegistrationForm()
    if form.validate_on_submit():
        # check whether member exists in the database and whether
        # the email entered matches the email in the database
        member = Member.query.filter_by(email=form.email.data).first()
        members = Member.query.all()
        for registered_member in members:
            if registered_member.department == form.department.data and registered_member.role == form.role.data:
                flash('That role in this department has already been taken')

                # redirect to register page
                return redirect(url_for('auth.leader_register'))
        if member is not None:
            if member.is_admin:
                abort(403)
            elif member.department == form.department.data and member.role == form.role.data:
                flash('Already registered as a leader in that department and role')
                # redirect to the login page
                return redirect(url_for('auth.login'))
            else:
                # if form.department.data == 
                member.department = form.department.data
                member.role = form.role.data
                member.password = form.password.data
                member.is_leader = True
                # updates detail of member in the database
                db.session.commit()
                flash('You have registered successfully')

                # redirect to the login page
                return redirect(url_for('auth.login'))

        # if email does not exist in database
        else:
            flash('You have not registered as a KUCU member')
            return redirect(url_for('auth.member_register'))

    # load the leader registration template
    return render_template('auth/leader_register.html', form=form, title='Leader Register', active="members")


@auth.route('/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        if current_user.is_admin:
            return redirect(url_for('home.admin_dashboard', active="dashboard"))
        else:
            check_role()
            role_name = check_role.role_name
            return redirect(url_for('home.{}_dashboard'.format(role_name.lower())))
    form = LoginForm()
    if form.validate_on_submit():
        # check whether member exists in the database and whether
        # the password entered matches the password in the database
        member = Member.query.filter_by(email=form.email.data).first()
        if member is not None and (member.is_leader or member.is_admin) and member.verify_password(form.password.data):
            login_user(member)

            # next = request.args.get('next')
            # if not is_safe_url(next):
            #     abort(403)
            # redirect to appropriate dashboard page after login
            if member.is_admin:
                return redirect(url_for('home.admin_dashboard', active="dashboard"))
            else:
                check_role()
                role_name = check_role.role_name
                return redirect(url_for('home.{}_dashboard'.format(role_name.lower())))

        # when details are wrong
        else:
            flash('Invalid email or password. You are not registered as a leader')

    # load login template
    return render_template('auth/login.html', form=form, title='Login')


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have logged out successfully')

    # redirect to the login page
    return redirect(url_for('auth.login'))

