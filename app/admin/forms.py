from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired
from wtforms_alchemy.fields import QuerySelectField

from ..models import Department, Role, DepartmentCategory


class DepartmentForm(FlaskForm):
    """For admin to CRUD department"""

    name = StringField('Name', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    category = SelectField('DepartmentCategory', choices=[('', 'Select')]+[(name, member.value) for name, member in DepartmentCategory.__members__.items()], validators=[DataRequired()])
    submit = SubmitField('Submit')


class RoleForm(FlaskForm):
    """For admin to add or edit a role"""
    name = StringField('Name', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    submit = SubmitField('Submit')



