# noinspection PyPackageRequirements
from flask import abort, flash, redirect, render_template, url_for
from flask_login import current_user, login_required

from .. import db
from ..models import Department, Member, Role
from .forms import DepartmentForm, RoleForm
from . import admin


def check_admin():
    """Prevent non-admins from accessing the page"""
    if not current_user.is_admin:
        abort(403)


@admin.route('/departments')
@login_required
def list_departments():
    """List all departments"""
    check_admin()

    departments = Department.query.all()

    return render_template('admin/departments/departments.html', departments=departments, title="Departments", active="departments")


@admin.route('/departments/add', methods=['GET', 'POST'])
@login_required
def add_department():
    """Add department to the database"""
    check_admin()

    add_department = True

    form = DepartmentForm()
    if form.validate_on_submit():
        department = Department(name=form.name.data, description=form.description.data, category=form.category.data)
        try:
            # add department to database
            db.session.add(department)
            db.session.commit()
            flash('You have successfully added a new department')
        except:
            # incase department name already exists
            flash('Error: department name already exists', 'warning')

        # redirect to departments page
        return redirect(url_for('admin.list_departments'))

    # load department template
    return render_template('admin/departments/department.html', action="Add", add_department=add_department, form=form, title="Add department", active="departments")


@admin.route('/departments/edit/<int:id>', methods=['GET', 'POST'])
def edit_department(id):
    """Edit a department"""
    check_admin()

    add_departments = False

    department = Department.query.get_or_404(id)
    form = DepartmentForm(obj=department)
    if form.validate_on_submit():
        department.name = form.name.data
        department.description = form.description.data
        department.category = form.category.data
        db.session.commit()
        flash('You have successfully edited the department')

        # redirect to departments page
        return redirect(url_for('admin.list_departments'))

    form.description.data = department.description
    form.name.data = department.name
    form.category.data = department.category
    return render_template('admin/departments/department.html', action="Edit", add_department=add_department, form=form, department=department, title="Edit Department", active="departments")


@admin.route('/departments/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_department(id):
    """Delete a department from the database"""
    check_admin()

    department = Department.query.get_or_404(id)
    db.session.delete(department)
    db.session.commit()
    flash('You have successfully deleted the department')

    # redirect to departments page
    return redirect(url_for('admin.list_departments'))

    return render_template(title="Delete Department")


@admin.route('/roles')
@login_required
def list_roles():
    check_admin()
    """List all roles"""
    roles = Role.query.all()
    return render_template('admin/roles/roles.html', roles=roles, title="Roles", active="roles")


@admin.route('/roles/add', methods=['GET', 'POST'])
@login_required
def add_role():
    """Add role to the database"""

    check_admin()

    add_role = True

    form = RoleForm()
    if form.validate_on_submit():
        role = Role(name=form.name.data, description=form.description.data)

        try:
            # add role to the database
            db.session.add(role)
            db.session.commit()
            flash('You have successfully added a new role')
        except:
            # incase role name already exists
            flash('Error: role name already exists', 'warning')

        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))

    # load role template
    return render_template('admin/roles/role.html', add_role=add_role, form=form, title="Add Role", active="roles")


@admin.route('/roles/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_role(id):
    """Edit a role"""
    check_admin()

    add_role = False

    role = Role.query.get_or_404(id)
    form = RoleForm(obj=role)
    if form.validate_on_submit():
        role.name = form.name.data
        role.description = form.description.data
        db.session.add(role)
        db.session.commit()
        flash('You have successfully edited the role')

        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))

    form.description.data = role.description
    form.name.data = role.name
    return render_template('admin/roles/role.html', add_role=add_role, form=form, title="Edit Role", active="roles")


@admin.route('/roles/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_role(id):
    """Delete a role from the database"""

    check_admin()

    role = Role.query.get_or_404(id)
    db.session.delete(role)
    db.session.commit()
    flash('You have successfully deleted the role')

    # redirect to the roles page
    return redirect(url_for('admin.list_roles'))

    return render_template(title="Delete Role")


@admin.route('/members')
@login_required
def list_members():
    """List all members"""
    check_admin()

    members = Member.query.all()
    return render_template('admin/members/members.html', members=members, title="Members", active="members")


@admin.route('/view/member/<int:id>')
@login_required
def view_member(id):
    """View a specific member"""
    check_admin()

    member = Member.query.get_or_404(id)
    return render_template('admin/members/member.html', member=member, id=member.id, active="members")

@admin.route('/makeadmin/member/<int:id>')
@login_required
def make_member_admin(id):
    """Give a member admin rights"""
    check_admin()

    member = Member.query.get_or_404(id)
    member.is_admin = True
    db.session.commit()
    return redirect(url_for('list_members'))

@admin.route('/revokeadminrights/member/<int:id>')
@login_required
def revoke_admin_rights(id):
    """Revoke admin rights from a member"""
    check_admin()

    member = Member.query.get_or_404(id)
    member.is_admin = False
    db.session.commit()
    return redirect(url_for('list_members'))
