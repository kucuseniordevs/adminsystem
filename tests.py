import unittest
import os

from flask import abort, url_for
from flask_testing import TestCase

from app import create_app, db
from app.models import Member, Department, Role, Budget, BudgetItem


class TestBase(TestCase):

    def create_app(self):

        # pass in test configurations
        config_name = 'testing'
        app = create_app(config_name)
        app.config.update(
                SQLALCHEMY_DATABASE_URI = 'mysql://kucuadmin:@kucuAdmin2018@localhost/adminsystem_test')
        return app

    def setUp(self):
        """Will be called before every test"""

        db.create_all()

        # create test admin user
        admin = Member(firstname="admin", password="admin2018", is_admin=True)

        # create test non-admin user
        member = Member(firstname="test_user", password="test2018")

        # save users to database
        db.session.add(admin)
        db.session.add(member)
        db.session.commit()

    def tearDown(self):
        """Will be called after every test"""
        db.session.remove()
        db.drop_all()


class TestModels(TestBase):

    def test_member_model(self):
        """Test the number of records in Member table"""
        self.assertEqual(Member.query.count(), 2)

    def test_department_model(self):
        """Test number of records in Dept table"""

        # create test department
        department = Department(name="IT", description="The IT Department")

        # save dept to database
        db.session.add(department)
        db.session.commit()

        self.assertEqual(Department.query.count(), 1)

    def test_role_model(self):
        """Test number of records in Roles table"""

        # create test role
        role = Role(name="CEO", description="Run the whole company")

        # save role to database
        db.session.add(role)
        db.session.commit()

        self.assertEqual(Role.query.count(), 1)

    def test_budget_model(self):
        """Test number of records in Budget table"""

        # create test role
        budget = Budget(name="TestBudget1", member_id=21, amount=1000)

        # save budget to database
        db.session.add(budget)
        db.session.commit()

        self.assertEqual(Budget.query.count(), 1)

    def test_budget_item_model(self):
        """Test number of records in BudgetItem table"""

        # create test role
        budget_item = BudgetItem(name="TestItem", category="TestCategory", price=100, quantity=10)

        # save budget_item to database
        db.session.add(budget_item)
        db.session.commit()

        self.assertEqual(BudgetItem.query.count(), 1)


class TestViews(TestBase):

    def test_login_view(self):
        """Test that login page is accessible without login"""
        response = self.client.get(url_for('auth.login'))
        self.assert200(response)

    def test_logout_view(self):
        """Test that logout link is inaccessible without login and redirects to login page then to logout"""
        target_url = url_for('auth.logout')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_treasurer_dashboard_view(self):
        """Test that treasurer dashboard is inaccessible without login and redirects to login page then to dashboard"""
        target_url = url_for('home.treasurer_dashboard')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_coordinator_dashboard_view(self):
        """
        Test that coordinator dashboard is inaccessible without login and redirects to login page then to dashboard
        """
        target_url = url_for('home.coordinator_dashboard')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_secretary_dashboard_view(self):
        """Test that secretary dashboard is inaccessible without login and redirects to login page then to dashboard"""
        target_url = url_for('home.secretary_dashboard')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_admin_dashboard_view(self):
        """
        Test that admin dashboard is inaccessible without login and redirects to login page then to admin dashboard
        """
        target_url = url_for('home.admin_dashboard')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_departments_view(self):
        """
        Test that departments page is inaccessible without login and redirects to login page then to departments page
        """
        target_url = url_for('admin.list_departments')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_roles_view(self):
        """Test that roles page is inaccessible without login and redirects to login page then to roles page"""
        target_url = url_for('admin.list_roles')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_members_view(self):
        """Test that members page is inaccessible without login and redirects to login page then to members page"""
        target_url = url_for('admin.list_members')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)

    def test_member_view(self):
        """Test that member page is inaccessible without login and redirects to login page then members page"""
        target_url = url_for('admin.view_member')
        redirect_url = url_for('auth.login', next=target_url)
        response = self.client.get(target_url)
        self.assertStatus(response, 302)
        self.assertRedirects(response, redirect_url)


class TestActions(TestBase):
    """Test various actions performed in the app"""

    def test_add_budget(self):
        """Test that a budget is added correctly and only if their are no unsubmitted budgets"""
        pass


class TestErrorPages(TestBase):

    def test_403_forbidden(self):
        # create route to abort the request with the 403 error
        @self.app.route('/403')
        def forbidden_error():
            abort(403)

        response = self.client.get('/403')
        self.assert403(response)
        # self.assertTrue("403 Error" in response.data)

    def test_404_not_found(self):
        response = self.client.get('/nothinghere')
        self.assert404(response)
        # self.assertTrue("404 Error" in response.data)

    def test_500_internal_server_error(self):
        # create route to abort the request with the 500 Error
        @self.app.route('/500')
        def internal_server_error():
            abort(500)

        response = self.client.get('/500')
        self.assert500(response)
        # self.assertTrue("500 Error" in response.data)


if __name__ == '__main__':
    unittest.main()

