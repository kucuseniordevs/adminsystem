class Config(object):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True

class ProductionConfig(Config):
    DEBUG = False

class DevelopmentConfig(Config):
    SQLALCHEMY_ECHO = True

class TestingConfig(Config):
    TESTING = True
    DEBUG = False

app_config = {
        'development':DevelopmentConfig,
        'production':ProductionConfig,
        'testing':TestingConfig
        }

