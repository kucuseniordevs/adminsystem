"""empty message

Revision ID: 8c15563b47ce
Revises: c0d84e9e238d
Create Date: 2018-11-19 22:04:30.852320

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8c15563b47ce'
down_revision = 'c0d84e9e238d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('budgets', sa.Column('is_submitted', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('budgets', 'is_submitted')
    # ### end Alembic commands ###
